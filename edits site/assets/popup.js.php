var City = new Array();
City[0] = "New York";
City[1] = "Los Angeles";
City[2] = "Chicago";
City[3] = "Philadelphia";
City[4] = "Phoenix";
City[5] = "San Antonio";
City[6] = "San Diego";
City[7] = "Dallas";
City[8] = "Austin";
City[9] = "San Francisco";
City[10] = "Indianapolis";
City[11] = "Columbus";
City[12] = "Seattle";
City[13] = "Detroit";
City[14] = "Washington";
City[15] = "Boston";
City[16] = "Springfield";
City[17] = "Ontario";
City[18] = "Richmond";
City[19] = "Las Vegas";

var catchText3;

var promoCount;

function showHydeWin(mode, vell){

	var showVell = vell;

	var theWindow = document.getElementById("popWindow");

	var opStart;
	var opEnd;

	if(mode === "+"){
		opStart = 0;
		opEnd = 1;
	}else if(mode === "-"){
		opStart = 1;
		opEnd = 0;
		showVell = -vell;
	}else{
		return;
	}

	var count = opStart;

	var showLoop = setInterval(function(){

		theWindow.style.opacity = count;
		count += showVell;

		if(mode === "+"){
			if(count > opEnd){
				theWindow.style.opacity = opEnd;
				clearInterval(showLoop);
			}
		}else if(mode === "-"){
			if(count < opEnd){
				theWindow.style.opacity = opEnd;
				clearInterval(showLoop);
				return;
			}
		}

	},17);

};

function genRandNum(min, max){
	var count = Math.floor((Math.random() * (max-min)) + min);
	return count;
};

function showBoxTime(sec, vell){
	showHydeWin("+", vell);
	var n = 0;
	var showLoop = setInterval(function(){
		if(n > sec){
			showHydeWin("-", vell);
			clearInterval(showLoop);
			return;
		}
		n ++;
	},1000);
};

function setBoxText(num){

	switch(num){
		case 1:
			var text = "At the moment there are 100 visitors on this site.";
			var count = genRandNum(150, 250);
			document.getElementById("popWindow").innerHTML = text.replace("100", "<span>"+count+"</span>");
			break;
		case 2:
			var text = "An order from CITY was just made.";
			var city = City[Math.floor(Math.random()*City.length)];
			document.getElementById("popWindow").innerHTML = text.replace("CITY", "<b>"+city+"</b>");
			break;
		case 3:
			var text = "15 promo price packages left!";
			var count = genRandNum(4, 8);
			catchText3 = count;
			document.getElementById("popWindow").innerHTML = text.replace("15", "<i>"+count+"</i>");
			break;
		default:
			return;
	}
	return;

};

function welcomeUser(){

	var time = 0;

	var custTime = genRandNum(9, 12);

	var userLoop = setInterval(function(){

		if(time === 0){
			setBoxText(1);
			showBoxTime(4, 0.05);
		}

		if(time === custTime){
			setBoxText(2);
			showBoxTime(4, 1);
		}

		if(time === custTime+7){
			setBoxText(3);
			promoCount = catchText3;
			showBoxTime(5, 0.01);
			clearInterval(userLoop);
			customLead(genRandNum(7, 20));
		}

		time ++;
	},1000);

};

function customLead(sec){

	setTimeout(function(){

		var time = 0;
		var leadLoop = setInterval(function(){

			if(time === 0){
				setBoxText(2);
				showBoxTime(4, 1);
			}

			if(time === 7){
				promoCount --;
				if(promoCount > 0){
					document.getElementById("popWindow").innerHTML = "There are only <i>"+promoCount+"</i> promo price packages left.";
				}else{
					document.getElementById("popWindow").innerHTML = "<p>If you order now, you will buy the product on a promotional price!!!</p>";
				}
				showBoxTime(5, 0.01);
			}

			if(time === 14){
				clearInterval(leadLoop);
				if(promoCount > 0){
					customLead(genRandNum(2, 20));
				}
			}

			time ++;
		},1000);

	},sec*1000);

};

