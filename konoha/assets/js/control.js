jQuery(function ($) {
  let scrollFromTop = 0;
  let scrollChange = 0;
  let menuState = 0;

  window.onscroll = (e) => {
    if ($(this).scrollTop() > 50) {
      scrollFromTop = 1;
      if (scrollFromTop !== scrollChange) {
        $('.header').addClass('header_onscroll');
        scrollChange = scrollFromTop;
      }
    } else {
      scrollFromTop = 0;
      if (scrollFromTop !== scrollChange) {
        $('.header').removeClass('header_onscroll');
        scrollChange = scrollFromTop;
      }
    }

  };

  $('.header__toggle').on('click', () => {
    if (menuState === 0) {
      $('.header__nav').addClass('header__nav_active');
      menuState = 1;
    } else {
      $('.header__nav').removeClass('header__nav_active');
      menuState = 0;
    }
  });
});